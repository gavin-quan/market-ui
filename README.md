# market-ui

## Project setup
```
npm install --registry=https://registry.npm.taobao.org
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Docker部署
需要修改docker/default.conf文件目录
```
npm install moduleName  --registry=https://registry.npm.taobao.org #安装模块到项目目录下
npm install -g moduleName  --registry=https://registry.npm.taobao.org #-g 的意思是将模块安装到全局，具体安装到磁盘哪个位置，要看 npm config prefix 的位置。
npm install -save moduleName  --registry=https://registry.npm.taobao.org #-save 的意思是将模块安装到项目目录下，并在package文件的dependencies节点写入依赖。
npm install -save-dev moduleName  --registry=https://registry.npm.taobao.org #-save-dev 的意思是将模块安装到项目目录下，并在package文件的devDependencies节点写入依赖。
npm install tronweb --registry=https://registry.npm.taobao.org
```