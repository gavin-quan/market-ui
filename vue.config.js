const FileManagerPlugin = require('filemanager-webpack-plugin')
module.exports = {
	publicPath: process.env.BASE_URL,
	runtimeCompiler: true,
	productionSourceMap: false,
	lintOnSave: false, //vue关闭严格模式
	configureWebpack: config => {
		if (process.env.NODE_ENV === 'production') {
			return {
				plugins: [
					new FileManagerPlugin({
						events: {
							onEnd: {
								delete: ["./target/app.tar"],
								archive: [{source: "./dist", destination: "./target/app.tar"}]
							},
						}
					})
				]
			}
		}
		return {
			devtool: '#eval-source-map',
			devServer: {
				open: false,
				disableHostCheck: true,
				proxy: "http://localhost:7103",
				port: 9080,
			}
		}
	}
}
