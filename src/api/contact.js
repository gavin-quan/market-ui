import { request, errorHandlers } from '@/utils/request.js'

export const list = () => request({handlers:[errorHandlers.auth()]}).get("/api-buy/contact")

export const info = (id) => request({handlers:[errorHandlers.auth()]}).get("/api-buy/contact/info?keyId=" + id)

export const del = (id) => request({handlers:[errorHandlers.auth()]}).delete("/api-buy/contact/" + id)

export const add = (form) => request({handlers:[errorHandlers.auth()]}).post("/api-buy/contact", form)

export const update = (id, form) => request({handlers:[errorHandlers.auth()]}).put("/api-buy/contact/" + id, form)

export const setDefault = (id) => request({handlers:[errorHandlers.auth()]}).put("/api-buy/contact/default?keyId=" + id)

export const getDefault = () => request({handlers:[errorHandlers.auth()]}).get("/api-buy/contact/default")