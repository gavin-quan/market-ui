import { request, errorHandlers } from '@/utils/request.js'

export const init = () => request({handlers:[errorHandlers.auth()]}).get('/api-market/init')
