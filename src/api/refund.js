import { request, errorHandlers } from '@/utils/request.js'

export const list = ( pages ) => request({handlers:[errorHandlers.auth()]}).get('/api-market/refunds', { params: pages.toData() })

export const detail = ( id ) => request({handlers:[errorHandlers.auth(), errorHandlers.pageNotFound()]}).get('/api-market/refunds/info?keyId=' + id)

export const create = ( id ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/refunds/create?orderKeyId=' + id)

export const cancel = ( id ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/refunds/cancel?refundOrderKeyId=' + id)