import { request, errorHandlers } from '@/utils/request.js'

export const list = ( pages ) => request().get('/api-buy/goods', { params: pages.toData() })

export const detail = ( id ) => request({handlers:[errorHandlers.pageNotFound()]}).get('/api-buy/goods/' + id)

export const content = ( id ) => request().get('/api-buy/goods/content/' + id)

export const skuList = ( id ) => request().get('/api-market/goods/sku/' + id)