import { request, errorHandlers } from '@/utils/request.js'

export const list = ( id ) => request({handlers:[errorHandlers.auth()]}).get("/api-buy/express?id=" + id)