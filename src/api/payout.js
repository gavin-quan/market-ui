import { request, errorHandlers } from '@/utils/request.js'

export const transfer = ( data ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/payout/transfer', data)

export const transfers = ( data ) => request({handlers:[errorHandlers.auth()]}).post('/api-market/payout/transfers', data)

export const cancel = ( data ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/payout/cancel', data)

export const get = () => request({handlers:[errorHandlers.auth()]}).get('/api-buy/payout')