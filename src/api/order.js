import { request, errorHandlers } from '@/utils/request.js'

export const detail = ( id ) => request({handlers:[errorHandlers.auth(), errorHandlers.pageNotFound()]}).get('/api-market/orders/info?keyId=' + id)

export const status = ( id ) => request({handlers:[errorHandlers.auth(), errorHandlers.pageNotFound()]}).get('/api-buy/orders/status?keyId=' + id)

export const delaySettle = (id) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/orders/delay?keyId=' + id )

export const confirmBySku = ( form ) => request({handlers:[errorHandlers.auth()]}).post('/api-market/orders/confirm/sku', form)

export const confirmByCart = ( form ) => request({handlers:[errorHandlers.auth()]}).post('/api-market/orders/confirm/cart', form)

export const confirmByOrder = ( orderId ) => request({handlers:[errorHandlers.auth()]}).post('/api-market/orders/confirm/order?orderId=' + orderId)

export const confirmExpress = ( orderId ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/orders/confirm?keyId=' + orderId)

export const create = ( form ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/orders/create', form)

export const prepay = ( form ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/orders/prepay', form)

export const pay = ( form ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/orders/pay', form)

export const directPay = ( form ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/orders/direct-pay', form)

export const orderPay = ( form ) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/orders/order-pay', form)

export const list = (pages) => request({handlers:[errorHandlers.auth()]}).get('/api-market/orders', { params: pages.toData() })

export const waitPays = (pages) => request({handlers:[errorHandlers.auth()]}).get('/api-market/orders/wait-pays', { params: pages.toData() })

export const waitDelivery = (pages) => request({handlers:[errorHandlers.auth()]}).get('/api-market/orders/wait-delivery', { params: pages.toData() })

export const waitShipped = (pages) => request({handlers:[errorHandlers.auth()]}).get('/api-market/orders/wait-shipped', { params: pages.toData() })

export const getInfoByBlockchainV1 = ( form ) => request({handlers:[errorHandlers.pageNotFound()]}).get("/api-pay/blockchain/pay/v1/" + form.orderNo + "?clientId=" + form.clientId)

export const doPublishByBlockchainV1 = (form) => request({handlers:[errorHandlers.pageNotFound()]}).post("/api-pay/blockchain/pay/v1/publish", form)

export const createByBlockchainV1 = (form) => request({handlers:[errorHandlers.pageNotFound()]}).post("/api-pay/blockchain/pay/v1/create", form)

export const getInfoByBlockchainV2 = ( form ) => request({handlers:[errorHandlers.pageNotFound()]}).get("/api-pay/blockchain/pay/v2/" + form.orderNo)

export const doPublishByBlockchainV2 = (form) => request({handlers:[errorHandlers.pageNotFound()]}).post("/api-pay/blockchain/pay/v2/publish", form)

export const createByBlockchainV2 = (form) => request({handlers:[errorHandlers.pageNotFound()]}).post("/api-pay/blockchain/pay/v2/create", form)
