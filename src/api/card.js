import { request, errorHandlers } from '@/utils/request.js'

export const list = () => request({handlers:[errorHandlers.auth()]}).post('/api-buy/card/list')

export const add = (form) => request({handlers:[errorHandlers.auth()]}).post('/api-buy/card', form)

export const del = (cardId) => request({handlers:[errorHandlers.auth()]}).delete('/api-buy/card/' + cardId)
