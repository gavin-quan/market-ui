import { request, errorHandlers } from '@/utils/request.js'
import store from '@/store'

export const login = (form) => request().post('/api-user/users/sign-in?app=' + store.getters.app + '&tenant=' + store.getters.tenant, form)

export const logout = () => request({handlers:[errorHandlers.auth()]}).post('/api-user/users/logout?app=' + store.getters.app + '&tenant=' + store.getters.tenant)

export const info = () => request({handlers:[errorHandlers.pageNotFound(process.env.VUE_APP_LOGIN_PATH)]}).get('/api-user/users/info?app=' + store.getters.app + '&tenant=' + store.getters.tenant)

export const statistic = () => request().get('/api-market/statistic')

export const openWallet = (form) => request({handlers:[errorHandlers.auth()]}).put('/api-user/users/wallet', form)

export const token = (tokenCode) => request().get('/api-user/users/token?tokenCode=' + tokenCode + '&app=' + store.getters.app + '&tenant=' + store.getters.tenant)

export const alipayLogin = (redirectUrl) => request().get('/api-user/alipay/sign-in?redirectUrl=' + redirectUrl + '&app=' + store.getters.app + '&tenant=' + store.getters.tenant)

export const weixinLogin = (redirectUrl) => request().get('/api-user/weixin/sign-in?redirectUrl=' + redirectUrl + '&app=' + store.getters.app + '&tenant=' + store.getters.tenant)

export const sendLoginSmsCode = (phoneNumber) => request().post('/api-user/sms/code?phone=' + phoneNumber + '&app=' + store.getters.app + '&tenant=' + store.getters.tenant)

export const loginBySmsCode = (form) => request().post('/api-user/sms/login?app=' + store.getters.app + '&tenant=' + store.getters.tenant, form)