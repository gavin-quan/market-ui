import { request, errorHandlers } from '@/utils/request.js'

export const count = (topicKeyId) => request().get("/api-comments/comments/count?topicKeyId=" + topicKeyId)

export const parentList = (keyId) => request().post("/api-comments/comments/parent/list", { keyId: keyId })

export const topicList = (topicKeyId) => request().post("/api-comments/comments/topic/list", { keyId: topicKeyId })

export const addParent = (form) => request({handlers:[errorHandlers.auth()]}).post("/api-comments/comments/parent", form)

export const addTopic = (form) => request({handlers:[errorHandlers.auth()]}).post("/api-comments/comments/topic", form)