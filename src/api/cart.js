import { request, errorHandlers } from '@/utils/request.js'

export const list = ( pages ) => request({handlers:[errorHandlers.auth()]}).get("/api-market/cart", {params: pages.toData()})

export const add = (form) => request({handlers:[errorHandlers.auth()]}).post("/api-buy/cart", form)

export const remove = (form) => request({handlers:[errorHandlers.auth()]}).delete("/api-buy/cart", {data: form})