import { request, errorHandlers } from '@/utils/request.js'
import qs from 'qs'

export const detail = (id) => request({handlers:[errorHandlers.auth(), errorHandlers.pageNotFound()]}).get('/api-pay/trans-orders/' + id)

export const pay = (form) => request({handlers:[errorHandlers.auth()]}).post('/api-pay/v1/trans-orders/wallet/pay', qs.stringify(form))

export const walletDetail = () => request({handlers:[errorHandlers.auth(), errorHandlers.pageNotFound("/wallet/open")]}).get('/api-buy/wallet')

export const changePassword = (form) => request({handlers:[errorHandlers.auth()]}).put('/api-buy/wallet/reset-password', form)

export const transfer = (form) => request({handlers:[errorHandlers.auth()]}).put('/api-buy/wallet/transfer', form)

export const billList = ( pages ) => request({handlers:[errorHandlers.auth()]}).get("/api-buy/wallet/bill/list", {params: pages.toData()})

export const topup = ( form ) => request({handlers:[errorHandlers.auth()]}).post("/api-buy/topup/pay", form)