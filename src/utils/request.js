import axios from 'axios'
import store from '@/store'
import router from '@/router'
import { loginPath } from '@/utils/env.js'

export const errorHandlers = {
	pageNotFound : ( page = "/404" ) => {
		return ( error ) => {
			if (error.response && error.response.data && error.response.data.code == "S1000002") {
				router.replace(page)
			}
		}
	},
	showError : () => {
		return ( error ) => {
			if (error.response && error.response.data) {
				this.$message('处理失败')
			}
		}
	},
	lostConnection: (page = "/502") => {
		return ( error ) => {
			if (error.request.status == 502 || error.request.status == 503) {
				router.replace(page)
			}
		}
	},
	auth : () => {
		return ( error ) => {
			if (error.response && error.response.data && error.response.data.code == "S1000008") {
				store.commit("user/SET_TOKEN", null, { root: true })
				if(window.location.href.indexOf(loginPath) == -1) {
					window.location.href = loginPath + "?redirect=" + encodeURIComponent(window.location.href)
				}
			}
		}
	}
}

let newRequest = (options = { handlers: null }) => {
	let instance = axios.create({
		baseURL: process.env.VUE_APP_BASE_API,
		timeout: 5000
	})
	instance.interceptors.request.use(
		config => {
			if (store.getters.token) {
				config.headers['token'] = store.getters.token
			}
			config.headers['ajax'] = "ajax"
			return config
		},
		error => {
			return Promise.reject(error)
		}
	)
	instance.interceptors.response.use(
		response => {
			if(response.data.success == false) {
				options.handlers && options.handlers.forEach((handler)=>{handler(({response: response}));})
				return Promise.reject(response)
			}
			return response.data
		},
		error => {
			options.handlers && options.handlers.forEach((handler)=>{handler((error));})
			return Promise.reject(error.response || error)
		}
	)
	return instance
}

export const request = newRequest