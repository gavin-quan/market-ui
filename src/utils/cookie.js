import Cookies from 'js-cookie'

export const getCookie = name => {
  return Cookies.get(name)
}

export const setCookie = (name, content) => {
  return Cookies.set(name, content)
}

export const removeCookie = (name) => {
  return Cookies.remove(name)
}