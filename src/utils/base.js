import router from '@/router'
import {Toast} from 'vant'
import {baseUrl} from '@/utils/env.js'
export default {
	install(Vue) {
		Vue.prototype.$goback = () => {
			window.history.length > 1 ? router.go(-1) : router.push('/')
		}
		Vue.prototype.$gohome = () => {
			router.push('/')
		}
		Vue.prototype.$getLableItem = ( value, itemList ) => {
			for (let index=0; index<itemList.length; index++) {
				let item = itemList[index]
				if(item.value == value) return item
			}
		}
		Vue.prototype.$href = (route) => {
			router.push(route || "/")
		}
		Vue.prototype.$baseUrl = () => {
			return window.location.origin + baseUrl
		}
		Vue.prototype.$clone = ( obj ) => {
			return JSON.parse(JSON.stringify(obj))
		}
		Vue.prototype.$showError = ( resp ) => {
			if(resp.status == 502 || resp.status == 503) {
				Toast("与服务器连接失败")
				return
			}
			let data = resp.data
			let error = Object.prototype.hasOwnProperty.call(data, "error") && data.error || {"msg" : data.msg}
			Object.keys(error).forEach((key)=>{
				setTimeout(()=>{ Toast(error[key]) }, 20)
			})
		}
	}
}
