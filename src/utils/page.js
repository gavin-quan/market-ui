class Pages {
	constructor( options ) {
		let def = { lazy : true, pageSize: 50, pageNum: 1, id: null }
		this.data = Object.assign(def, options || {})
		this.pageSizes = [50, 100]
	}
	getPageSize() {
		return this.data.pageSize
	}
	setPageSize( pageSize ) {
		this.data.pageSize = pageSize
	}
	setId( id ) {
		this.data .id = id
	}
	setPageNum( pageNum ) {
		this.data.pageNum = pageNum
	}
	next() {
		this.data.pageNum++
	}
	toData() {
		return Object.assign(this.data, { pageSize: this.data.lazy ? null : this.data.pageSize })
	}
	reset() {
		this.data.pageNum = 1
	}
}

export default Pages
