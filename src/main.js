import Vue from 'vue'
import Vant from 'vant'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'vant/lib/index.css'
import '@/utils/rem.js'
import '@/styles/icon/iconfont.js'
import router from '@/router'
import store from '@/store'
import base from '@/utils/base.js'
import Vconsole from 'vconsole';
if (process.env.NODE_ENV !== 'production') {
  new Vconsole();
}
Vue.use(Vant)
Vue.use(ElementUI);
Vue.use(base);
new Vue({
	router,
	store
}).$mount('#app')
