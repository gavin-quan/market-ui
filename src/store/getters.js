export default {
	token : state => state.user.token,
	app: state => state.user.app,
	tenant: state => state.user.tenant,
	userInfo: state => state.user.userInfo,
	statistic: state => state.user.statistic,
	selectedContactId: state => state.order.selectedContactId,
	global: state => state.global.data,
}