import { login, info, statistic, logout, token, loginBySmsCode } from '@/api/user.js'
import { getCookie, setCookie, removeCookie } from '@/utils/cookie.js'
import qs from 'qs'

const COOKIE_TOKEN = "token_" + process.env.VUE_APP_APP_CODE

const state = {
	token: getCookie(COOKIE_TOKEN),
	userInfo: {},
	app: process.env.VUE_APP_APP_CODE,
	tenant: process.env.VUE_APP_TENANT_CODE || qs.parse(location.search.substring(1)).tenant || 'default',
	statistic: {}
}

const mutations = {
	SET_TOKEN(state, data) {
		state.token = data
		if(!data) {
			removeCookie(COOKIE_TOKEN)
		} else {
			setCookie(COOKIE_TOKEN, data)
		}
	},
	SET_USER_INFO(state, data) {
		state.userInfo = data
	},
	SET_STATISTIC(state, data) {
		state.statistic = data
	}
}

const actions = {
	login({ commit }, form) {
		return new Promise((resolve, reject) => {
			login(form).then(response => {
				const { data } = response
				commit('SET_TOKEN', data.token)
				resolve(response)
			}).catch(error => {
				reject(error)
			})
		})
	},
	smslogin({ commit }, form) {
		return new Promise((resolve, reject) => {
			loginBySmsCode(form).then(response => {
				const { data } = response
				commit('SET_TOKEN', data.token)
				resolve(response)
			}).catch(error => {
				reject(error)
			})
		})
	},
	async token({ commit }, tokenCode) {
		await token(tokenCode).then(response => {
			const { data } = response
			if(data && data.token) {
				commit('SET_TOKEN', data.token)
			}
			return response
		}).catch(error => {
			throw error
		})
	},
	info({commit}) {
		return new Promise((resolve, reject) => {
			info().then(response=>{
				const { data } = response
				commit('SET_USER_INFO', data)
				resolve(response)
			}).catch(error=>{
				reject(error)
			})
		})
	},
	statistic({commit}) {
		return new Promise((resolve, reject) => {
			statistic().then(response=>{
				const { data } = response
				commit('SET_STATISTIC', data)
				resolve(response)
			}).catch(error=>{
				reject(error)
			})
		})
	},
	logout({commit}) {
		return new Promise((resolve, reject) => {
			logout().then(response=>{
				commit('SET_TOKEN', null)
				resolve(response)
			}).catch(error=>{
				reject(error)
			})
		})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
