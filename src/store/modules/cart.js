import { add } from '@/api/cart.js'

let state = {

}

let mutations = {

}

let actions = {
	add({dispatch}, form ) {
		return new Promise((resolve, reject)=>{
			add(form).then(resp=>{
				dispatch('user/statistic', null, { root: true })
				resolve(resp)
			}).catch(error=>{
				reject(error)
			})
		})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
