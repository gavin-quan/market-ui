const state = {
	selectedContactId: null
}

const mutations = {
	SET_CONTACT_ID (state, contactId) {
		state.selectedContactId = contactId
	}
}

const actions = {
	
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}