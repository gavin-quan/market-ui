import { init } from '@/api/app.js'

const state = {
	data: {},
}

const mutations = {
	SET_INIT(state, data) {
		state.data = data
	}
}

const actions = {
	init({commit}) {
		return new Promise((resolve, reject) => {
			init().then(response=>{
				const { data } = response
				commit('SET_INIT', data)
				resolve(response)
			}).catch(error=>{
				reject(error)
			})
		})
	},
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
