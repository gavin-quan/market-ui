import App from '../App.vue'

const routes = [{
	path: '/',
	component: App,
	children: [{
		path: '',
		redirect: "/index"
	},{
		path: '/login',
		component: () => import("@/views/user/login.vue"),
		meta: { title: '登录'}
	}, {
		path: '/index',
		component: () => import("@/views/goods/list.vue"),
		meta: { title: '首页'}
	}, {
		path: '/goods',
		component: () => import("@/views/goods/detail.vue"),
		meta: { title: '详情'}
	}, {
		path: '/order/confirm',
		name: 'orderConfirm',
		component: () => import("@/views/order/confirm.vue"),
		meta: { title: '订单确认', requireAuthenticated: true}
	}, {
		path: '/order',
		component: () => import("@/views/order/list.vue"),
		meta: { title: '订单列表', requireAuthenticated: true}
	}, {
		path: '/order/status',
		component: () => import('@/views/order/status.vue'),
		meta: { title: '订单状态'}
	}, {
		path: '/order/detail',
		component: () => import('@/views/order/detail.vue'),
		meta: { title: '订单详情', requireAuthenticated: true}
	}, {
		path: '/express',
		component: () => import('@/views/express/list.vue'),
		meta: { title: '物流跟踪', requireAuthenticated: true}
	},{
		path: '/order/pay',
		component: () => import('@/views/order/pay.vue'),
		meta: { title: '订单付款', requireAuthenticated: true}
	}, {
		path: '/contact',
		component: () => import('@/views/contact/list.vue'),
		meta: { title: '联系地址', requireAuthenticated: true}
	}, {
		path: '/contact/edit',
		component: () => import('@/views/contact/edit.vue'),
		meta: { title: '编辑地址', requireAuthenticated: true}
	}, {
		path: '/contact/add',
		component: () => import('@/views/contact/add.vue'),
		meta: { title: '增加地址', requireAuthenticated: true}
	}, {
		path: '/contact/select',
		component: () => import('@/views/contact/select.vue'),
		meta: { title: '选择地址', requireAuthenticated: true}
	}, {
		path: '/cart',
		component: () => import('@/views/cart/list.vue'),
		meta: { title: '购物车', requireAuthenticated: true}
	}, {
		path: '/settings',
		component: () => import('@/views/user/settings.vue'),
		meta: { title: '设置'}
	}, {
		path: '/refund',
		component: () => import('@/views/refund/list.vue'),
		meta: { title: '退款', requireAuthenticated: true}
	}, {
		path: '/refund/detail',
		component: () => import('@/views/refund/detail.vue'),
		meta: { title: '退款详情', requireAuthenticated: true}
	}, {
		path: '/wallet',
		component: () => import('@/views/wallet/detail.vue'),
		meta: { title: '我的钱包', requireAuthenticated: true}
	}, {
		path: '/wallet/pay',
		component: () => import('@/views/wallet/pay.vue'),
		meta: { title: '钱包付款', requireAuthenticated: true}
	}, {
		path: '/wallet/bill',
		component: () => import('@/views/wallet/bill.vue'),
		meta: { title: '账单列表', requireAuthenticated: true}
	}, {
		path: '/wallet/open',
		component: () => import('@/views/wallet/open.vue'),
		meta: { title: '开通钱包', requireAuthenticated: true}
	}, {
		path: '/wallet/password',
		component: () => import('@/views/wallet/password.vue'),
		meta: { title: '修改密码', requireAuthenticated: true}
	},{
		path: '/wallet/transfer',
		component: () => import('@/views/wallet/transfer.vue'),
		meta: { title: '内部转账', requireAuthenticated: true}
	},{
		path: '/wallet/topup',
		component: () => import('@/views/wallet/topup/index.vue'),
		meta: { title: '钱包充值', requireAuthenticated: true}
	},{
		path: '/wallet/topup/status',
		component: () => import('@/views/wallet/topup/status.vue'),
		meta: { title: '充值结果', requireAuthenticated: true}
	},{
		path: '/wallet/payout',
		component: () => import('@/views/wallet/payout.vue'),
		meta: { title: '申请提现', requireAuthenticated: true}
	},{
		path: '/wallet/card',
		component: () => import('@/views/wallet/card.vue'),
		meta: { title: '我的卡片', requireAuthenticated: true}
	},{
		path: '/wallet/payouts',
		component: () => import('@/views/wallet/payouts.vue'),
		meta: { title: '提现历史', requireAuthenticated: true}
	},{
		path: '/about',
		component: () => import('@/views/user/about.vue'),
		meta: { title: '关于我们' }
	}, {
		path: '/500',
		component: () => import('@/views/commons/500.vue'),
		meta: { title: '服务有些问题'}
	}, {
		path: '/404',
		component: () => import('@/views/commons/404.vue'),
		meta: { title: '数据不见了'}
	}, {
		path: '*',
		component: () => import('@/views/commons/404.vue'),
		meta: { title: '404'}
	}]
}]

export default routes
