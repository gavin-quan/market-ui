import Vue from 'vue'
import routes from './router.js'
import { loginPath } from '@/utils/env.js'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const router = new VueRouter({
	routes,
	mode: process.env.VUE_APP_ROUTER_MODE,
	base: process.env.BASE_URL,
	strict: process.env.NODE_ENV !== 'production',
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition
		} else {
			if (from.meta.keepAlive) {
				from.meta.savedPosition = document.body.scrollTop;
			}
			return {
				x: 0,
				y: to.meta.savedPosition || 0
			}
		}
	}
})

router.beforeEach(async (to, from, next) => {
	let hasToken = store.getters.token
	document.title = to.meta.title
	if (to.query.tokenCode) {
		await store.dispatch('user/token', to.query.tokenCode)
	}
	if (hasToken) {
		next()
	} else {
		if(to.meta.requireAuthenticated) {
			next(`${loginPath}?redirect=${encodeURIComponent(window.location.href)}`)
		} else {
			next()
		}
	}
})

export default router
